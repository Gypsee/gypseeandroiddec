package com.gypsee.reader.io;

public interface ObdProgressListener {

    void stateUpdate(final ObdCommandJob job);

}